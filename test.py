from twelvedays import *

xmas = TwelveDays()
days = 12
day = 1

print(xmas.getFirstLine(1))
print(xmas.getGift(1))
while day < days:
    day += 1
    verse = xmas.getFirstLine(day)
    n = day
    while n > 1:
        verse += xmas.getGift(n)
        n -= 1

    verse += "and "
    verse += xmas.getGift(1)
    verse += "\n"
    print(verse)

print("")
